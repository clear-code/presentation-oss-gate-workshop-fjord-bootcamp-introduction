# 今日から参加できる！OSS開発

[フィヨルドブートキャンプさん](https://bootcamp.fjord.jp/)との共催による[OSS Gateオンラインワークショップ - フィヨルドブートキャンプ特別版](https://oss-gate.doorkeeper.jp/events/160648)での発表資料です。

## ライセンス

CC BY-SA 4.0

詳細は[LICENSE](./LICENSE)を確認してください。

## スライド

### 表示

```console
% rake
```

### 公開

```console
% rake publish
```
